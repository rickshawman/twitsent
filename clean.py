import json


with open('dump.txt') as data_file:    
    raw_data = json.load(data_file)
count = 0
tweets_json = {"tweets": []}
for tweet in raw_data['twitter']:
	if count >= 50000:
			break
	temp_dict = {'id': "",'text': ""}
	if tweet.has_key("text"):
		count = count + 1
		temp_dict['id'] 	= tweet['id_str']
		temp_dict['text'] 	= tweet['text']
		tweets_json['tweets'].append(temp_dict)

fp = open('tweets.json','w')
fp.write(json.dumps(tweets_json))
