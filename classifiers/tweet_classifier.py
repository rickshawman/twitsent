import ast
import nltk
import csv
import json
import nltk, re, pprint
import pdb
import collections
import pickle
from nltk.corpus import stopwords
from progressbar import ProgressBar
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.stem.lancaster import LancasterStemmer

def create_tuples( file_name ):
	result = []
	with open(file_name, 'rb') as f:
		reader = csv.reader(f)
		pbar = ProgressBar()
		for row in reader:
			text = ""
			try:
				text = convert(row[5])
			except UnicodeEncodeError:
				print "unicoderescue"
				continue
			result.append((row[5],row[0]))
	#print result
	return result

def preprocess(tweet):
	try:
		lm = WordNetLemmatizer()
		st = LancasterStemmer()
		#pdb.set_trace()
		tweet = tweet.lower()
		words = nltk.word_tokenize(tweet)
		words = [i for i in words if i not in stopwords.words('english')]
		pbar = ProgressBar()
		for word in words:
			word = lm.lemmatize(word)
			word = st.stem(word)
	except UnicodeWarning:
		return ['']
	#print words
	return words

def get_words_in_tweets(tweets):
	all_words = []
	for(words,sentiment) in tweets:
		all_words.extend(words)
	return all_words

def get_word_features(wordlist):
	wordlist = nltk.FreqDist(wordlist)
	word_features = wordlist.keys()
	return word_features

def extract_features(document):
	document_words = set(document)
	features = {}
	for word in word_features:
		features['contains(%s)' % word] = (word in document_words)
	return features

def convert(data):
    if isinstance(data, basestring):
        return str(data)
    elif isinstance(data, collections.Mapping):
        return dict(map(convert, data.iteritems()))
    elif isinstance(data, collections.Iterable):
        return type(data)(map(convert, data))
    else:
        return data

pbar = ProgressBar()

tweets = create_tuples('twitter140/training.csv')
tweets_filtered = []
count = 0

print "PREPROCESSING TRAINING SET"
for tweet in pbar(tweets):
	count = count + 1
	if count > 100:
		break
	temp=preprocess(tweet[0])
	tweets_filtered.append((temp,tweet[1]))

print "TRAINING CLASSIFIER"
word_features = get_word_features(get_words_in_tweets(tweets_filtered))
training_set = nltk.classify.apply_features(extract_features,tweets)
classifier = nltk.NaiveBayesClassifier.train(training_set)

print "SAVING CLASSIFIER"
fp = open('twitter_classifier_copy.pickle','wb')
pickle.dump(classifier,fp)
fp.close()


print "TESTING"
tweets = create_tuples('twitter140/testing.csv')
tweets_filtered = []
count =0
for tweet in pbar(tweets):
	count = count + 1
	print count
	if count > 10:
		break
	temp=preprocess(tweet[0])
	tweets_filtered.append((temp,tweet[1]))

testing_set = nltk.classify.apply_features(extract_features,tweets)
print(nltk.classify.accuracy(classifier, testing_set))
fp = open('accuracy_new.txt','w')
fp.write(str(nltk.classify.accuracy(classifier, testing_set)))
fp.close()
