import json
import nltk, re, pprint
import pdb
import collections
import pickle
import unirest
import numpy as np 
import matplotlib.pyplot as plt
from nltk.corpus import sentiwordnet as sn 
from progressbar import ProgressBar
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords

def preprocess(tweet):
	lemmatizer = WordNetLemmatizer()
	sentences = nltk.sent_tokenize(tweet)
	sentences = [nltk.word_tokenize(sent) for sent in sentences]
	res_sent = []
	result = []
	for sent in sentences:
		for word in sent:
			word = lemmatizer.lemmatize(word)
			#word = word.lower()
			if word.lower() not in stopwords.words('english'):
				if word.lower() not in ['!','@','#','$','%','^','&','*','(',')','<','>','/','?',':',';','\"','\'','{','}','[',']','/','~','`','=','+','-','_','.',',','get','updates','found','channel']:
					res_sent.append(word)
		result.append(res_sent)
	position = 0
	result = [nltk.pos_tag(sent) for sent in result]
	position = 1
	return result

def ner(sentences):
	result = []
	for sentence in sentences:
		result.append(nltk.ne_chunk(sentence))
	return result

def find_ne_trees(sentences,ne):
	for sentence in sentences:
		for t in sentence.subtrees():
			if t.label() == 'PERSON' or t.label() == 'LOCATION' :
				for word in t.leaves():
					if word[0] in ne:
						ne[word[0]] = ne[word[0]] + 1
					else:
						ne[word[0]] = 1
	return ne

def convert(data):
    if isinstance(data, basestring):
        return str(data)
    elif isinstance(data, collections.Mapping):
        return dict(map(convert, data.iteritems()))
    elif isinstance(data, collections.Iterable):
        return type(data)(map(convert, data))
    else:
        return data

def analyse_news(entity):
	
	print "Analysing entity: " + entity
	with open('news/'+entity+'.json','r') as data_file:    
		news = json.load(data_file)
    	overall_pos = 0
    	overall_neg = 0
    	res = []
    	pbar = ProgressBar()
    	count = 0
    	for item in pbar(news['posts']):
    		count = count +1
    		neg = 0
    		pos = 0
    		x = item['text']
    		x = preprocess(x)
    		for sentence in x:
    			for word in sentence:
    				sent_object = sn.senti_synsets(word[0])
    				if len(sent_object) > 0:
    					pos = pos + sent_object[0].pos_score()
    					neg = neg + sent_object[0].neg_score()
    		if pos > neg:
    			res.extend("1")
    		elif pos == neg:
    			res.extend("0")
    		else:
    			res.append("-1")
    		overall_neg = overall_neg + neg
    		overall_pos = overall_pos + pos
    	if overall_pos > overall_neg:
    		return 'P',res
    	elif overall_pos == overall_neg: 
    		return 'Nu',res
    	else:
    		return 'N',res

def get_word_features(wordlist):
	wordlist = nltk.FreqDist(wordlist)
	word_features = wordlist.keys()
	return word_features

def extract_features(document):
	features = {}
	for word in document:
		features[word] = True
	return features

def get_words_in_tweets(tweets):
	all_words = []
	for tweet in tweets:
		for(words,sentiment) in tweet:
			all_words.append(words)
	return all_words

def analyse_tweets(entity,tweets,classifier):
	
	tweets_array = []
	result = []
	pos = 0
	neg = 0
	for tweet in tweets['tweets']:
		words = get_words_in_tweets(tweet['words'])
		if entity in words:
			tweets_array.append(words)
	sentiment = []
	for tweet in tweets_array:
		sent = classifier.classify(extract_features(tweet))
		if sent == 'neg':
			sentiment.append('-1')
			neg = neg + 1
		elif sent == 'pos':
			sentiment.append('1')
			pos = pos + 1
	if pos > neg:
		return sentiment, 'Positive'
	elif pos == neg:
		return sentiment, 'Neutral'
	else:
		return sentiment, 'Negative'

def autolabel(rects,ax):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')

def graph_plot(N,pos,neg,entity,title):
	posScores = (pos[0], pos[1], pos[2], pos[3], pos[4])
	#menStd = (2, 3, 4, 1, 2)

	ind = np.arange(N)  # the x locations for the groups
	width = 0.35       # the width of the bars

	fig, ax = plt.subplots()
	rects1 = ax.bar(ind, posScores, width, color='r')

	negScores = (neg[0],neg[1],neg[2],neg[3],neg[4])
	#womenStd = (3, 5, 2, 3, 3)
	rects2 = ax.bar(ind + width, negScores, width, color='y')

	# add some text for labels, title and axes ticks
	ax.set_ylabel('Total Number of ' + str(title))
	ax.set_title('Scores by entity')
	ax.set_xticks(ind + width)
	ax.set_xticklabels((entity[0],entity[1],entity[2],entity[3],entity[4]))

	ax.legend((rects1[0], rects2[0]), ('Positive', 'Negative'))
	autolabel(rects1,ax)
	autolabel(rects2,ax)
	plt.show()
	plt.savefig(str(title)+'.png')

def count_sent(arr,sr):
	count = 0
	for i in arr:
		if i == sr:
			count = count + 1
	return count

with open('tweets.json') as data_file:    
    raw_tweets = json.load(data_file)

named_entities= {}
count = 0
pbar = ProgressBar()

print "Finding Named Entitites"

limit = 20000
filtered_tweets = {"tweets": []}
for tweet in pbar(raw_tweets['tweets']):
	filtered_tweet = {}
	tweet['text'] = preprocess(tweet['text'])
	try:
		tweet['text'] = convert(tweet['text'])
	except UnicodeEncodeError:
		continue
	count = count + 1
	filtered_tweet['text'] = ner(tweet['text'])
	filtered_tweet['words'] = tweet['text']
	filtered_tweets['tweets'].append(filtered_tweet)
	named_entities = find_ne_trees(filtered_tweet['text'],named_entities)
	if count == limit:
		break

named_entities = sorted(named_entities.items(), key=lambda x: x[1], reverse=True)
top_5 = named_entities[:5]
unirest.timeout(240)

print "Top 5 entities are: " + str(top_5)
print "Finding News"
pbar = ProgressBar()
for entity in pbar(top_5):
	request = "https://webhose.io/search?token=9baa2ae9-3922-44e9-b7e7-035c645ee2c4&format=json&q="+entity[0]+"%20language%3A(english)%20performance_score%3A%3E0&ts=1454842233151"
	response = unirest.get(request,headers={ "Accept": "text/ascii"	})
	fp = open("news/"+entity[0]+".json","w")
	json.dump(response.body,fp)
	fp.close()

fp = open('classifiers/classifier-NaiveBayes.tweets.pickle','rb')
tweet_classifier = pickle.load(fp)
fp.close()


print "Analysing news"
pbar = ProgressBar()
count = 0
t_pos = [0,0,0,0,0]
t_neg = [0,0,0,0,0]
n_pos = [0,0,0,0,0]
n_neg = [0,0,0,0,0]
entities = []
#pdb.set_trace()
for entity in top_5:
	entities.append(entity[0])
	news_sentiment = ""
	tweet_sentiment = ""
	news_sentiment,news_dist  = analyse_news(str(entity[0]))
	tweet_dist, tweet_sentiment = analyse_tweets(str(entity[0]),filtered_tweets,tweet_classifier)
	n_pos[count] = count_sent(news_dist,'1')
	n_neg[count] = count_sent(news_dist,'-1')
	t_pos[count] = count_sent(tweet_dist,'1')
	t_neg[count] = count_sent(tweet_dist,'-1')
	#pdb.set_trace()
	count=count+1

graph_plot(5,t_pos,t_neg,entities,'Tweets')
graph_plot(5,n_pos,n_neg,entities,'News')
